resource "google_compute_address" "boundary" {
  count = var.instance_count

  name         = "${var.cluster_name}-internal-${count.index}"
  address_type = "INTERNAL"
  region       = var.region
}
