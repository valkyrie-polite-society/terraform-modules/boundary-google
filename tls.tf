resource "tls_private_key" "boundary" {
  count = var.instance_count

  algorithm   = "ECDSA"
  ecdsa_curve = "P384"
}

resource "tls_cert_request" "boundary" {
  count = var.instance_count

  key_algorithm   = "ECDSA"
  private_key_pem = tls_private_key.boundary[count.index].private_key_pem

  subject {
    common_name  = "Boundary Server"
    organization = "Valkyrie Polite Society"
  }

  ip_addresses = [
    "127.0.0.1",
    google_compute_address.boundary[count.index].address,
  ]

  dns_names = [
    "localhost",
    trimsuffix("boundary.${data.google_dns_managed_zone.personal.dns_name}", ".")
  ]
}

resource "tls_locally_signed_cert" "boundary" {
  count = var.instance_count

  ca_key_algorithm   = "ECDSA"
  cert_request_pem   = tls_cert_request.boundary[count.index].cert_request_pem
  ca_private_key_pem = data.terraform_remote_state.ca.outputs.intermediate_ca_private_key_pem
  ca_cert_pem        = data.terraform_remote_state.ca.outputs.intermediate_ca_certificate_pem

  validity_period_hours = 480

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
    "client_auth",
  ]
}

data "terraform_remote_state" "ca" {
  backend = "gcs"

  config = {
    bucket = var.certificate_authority_remote_state_bucket
    prefix = var.certificate_authority_remote_state_prefix
  }
}
