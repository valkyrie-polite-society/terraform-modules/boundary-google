controller {
  name = "${name}"

  database {
    url = "${database_url}"
  }
}
