# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/google" {
  version     = "3.51.1"
  constraints = "~> 3.51.0"
  hashes = [
    "h1:EgPXM0JC+mSEuqQgQh9JDClpfSbQFqTEX3Xv62pdmeU=",
    "zh:112dac830ab1c0c0684c96a32e1f624a789ab7ccd613e22d42f09b6b41fc5a74",
    "zh:392b1a0236800bc00a4b7c3836a13aa01d1f45e7e250dca05d436e80765e8619",
    "zh:3fd98d206ae0cb61f287af6dcbdc206531e70d378157b33d1f01fbcecf74512c",
    "zh:43bcbf008710f0a11f62740b2fbee8fe2303667a535c15cc93119f965daeb698",
    "zh:69fb8d60c08ef6dbb5b7c2f191c010572a76ba0f58297c97bbfe8246482dbea9",
    "zh:8e9f094e516e26cc8ac121c13cb796f3f303f5ae74285928d18fde36b7fa1cb2",
    "zh:aaf9e760921f09a1d0c21261b1407a68c4d7c3e90530b0cd90ae0a87ef620388",
    "zh:ac11e0bdce4ddf06aae6619a6a7b43e46922f097bf51f9919683f8ab22e4d171",
    "zh:c9e8f8e8b635e813b46f36c7659b6a4aec32a1b359078273076c146d578b541f",
    "zh:d31f870f84c2b9d95253343855102662a09ab2514a51cc9af63a73512551cdbb",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version = "3.0.0"
  hashes = [
    "h1:grDzxfnOdFXi90FRIIwP/ZrCzirJ/SfsGBe6cE0Shg4=",
    "zh:0fcb00ff8b87dcac1b0ee10831e47e0203a6c46aafd76cb140ba2bab81f02c6b",
    "zh:123c984c0e04bad910c421028d18aa2ca4af25a153264aef747521f4e7c36a17",
    "zh:287443bc6fd7fa9a4341dec235589293cbcc6e467a042ae225fd5d161e4e68dc",
    "zh:2c1be5596dd3cca4859466885eaedf0345c8e7628503872610629e275d71b0d2",
    "zh:684a2ef6f415287944a3d966c4c8cee82c20e393e096e2f7cdcb4b2528407f6b",
    "zh:7625ccbc6ff17c2d5360ff2af7f9261c3f213765642dcd84e84ae02a3768fd51",
    "zh:9a60811ab9e6a5bfa6352fbb943bb530acb6198282a49373283a8fa3aa2b43fc",
    "zh:c73e0eaeea6c65b1cf5098b101d51a2789b054201ce7986a6d206a9e2dacaefd",
    "zh:e8f9ed41ac83dbe407de9f0206ef1148204a0d51ba240318af801ffb3ee5f578",
    "zh:fbdd0684e62563d3ac33425b0ac9439d543a3942465f4b26582bcfabcb149515",
  ]
}

provider "registry.terraform.io/hashicorp/tls" {
  version = "3.0.0"
  hashes = [
    "h1:LtCEW5v1E5Eo49+kQOsKHRYf9Hc8ZR0jTpK+mXszPHs=",
    "zh:05eac573a1fe53227bcc6b01daf6ddf0b73456f97f56f316f1b3114a4771e175",
    "zh:09390dad764c76f0fd59cae4dad296e3e39487e06de3a4bc0df73916c6bb2f17",
    "zh:142d0bc4722ab088b7ca124b0eb44206b9d100f51035c162d50ef552e09813d0",
    "zh:2c391743dd20f43329c0d0d49dec7827970d788115593c0e32a57050c0a85337",
    "zh:525b12fc87369c0e6d347afe6c77668aebf56cfa078bb0f1f01cc2ee01ac7016",
    "zh:5583d81b7a05c6d49a4c445e1ee62e82facb07bb9204998a836b7b522a51db8d",
    "zh:925e3acc70e18ed1cd296d337fc3e0ca43ac6f5bf2e660f24de750c7754f91aa",
    "zh:a291457d25b207fd28fb4fad9209ebb591e25cfc507ca1cb0fb8b2e255be1969",
    "zh:bbf9e2718752aebfbd7c6b8e196eb2e52730b66befed2ea1954f9ff1c199295e",
    "zh:f4b333c467ae02c1a238ac57465fe66405f6e2a6cfeb4eded9bc321c5652a1bf",
  ]
}
