resource "google_service_account" "controller" {
  account_id   = "${var.cluster_name}-controller"
  display_name = "Boundary Controller - ${var.cluster_name}"
}

resource "google_kms_crypto_key_iam_binding" "root" {
  crypto_key_id = local.external_resources.root_kms_key.id
  role          = "roles/cloudkms.cryptoKeyEncrypterDecrypter"
  members       = ["serviceAccount:${google_service_account.controller.email}"]
}

resource "google_kms_crypto_key_iam_binding" "worker_auth" {
  crypto_key_id = local.external_resources.worker_auth_kms_key.id
  role          = "roles/cloudkms.cryptoKeyEncrypterDecrypter"
  members       = ["serviceAccount:${google_service_account.controller.email}"]
}

resource "google_project_iam_member" "cloud_sql" {
  project = var.project
  role    = "roles/cloudsql.client"
  member  = "serviceAccount:${google_service_account.controller.email}"
}
